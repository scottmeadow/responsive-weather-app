# Reponsive Weather App

"Build a single page web app displaying the 5 day weather forecast."
http://weatherapp.scottmeadow.com/

## Technology stack

* Angular
* Semantic-UI
* Angular-cli
* [OpenWeatherMap forecast](http://openweathermap.org)

## Building
Install angular cli

```
npm install -g @angular/cli
```

Install all dependencies
```
npm install

```

Install all dependencies
```
npm install

```

Run the app
```
ng serve

```

This will start up the app in development mode.
Navigate to [http://localhost:4200](http://localhost:4200)


## Deployment

To build the app for production, run:
```
ng build --env==prod
```

## Thoughts

I used angular-cli even it is a globally installed dependency as Angular recommends for easy configuration and setup


# If I had more time...

### Search for location
A nice feature would be the ability to search for a location.  At the moment its hardcoded to New York.

### Background Animation
A moving background based on the current weather
