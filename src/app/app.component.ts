import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import * as moment from 'moment';

interface Weather {
    dt: number,
    temp: {
      day: number,
      min: number,
      max: number,
      night: number,
      eve: number,
      morn: number
    },
    pressure: number,
    humidity: number,
    weather: any[],
    speed: number,
    deg: number,
    clouds: number
}

interface City {
  id: number,
  name: string,
  coord: {
    lon: number,
    lat: number
  },
  country: string,
  population: number
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  currentWeather: Weather;
  weathers = [];
  city: City;
  currentDate;
  currentTime;

  constructor( private http: Http ) {
    this.getWeatherData().subscribe( data => {

       // Get the current weather info
       this.currentWeather = data.list[0];

      // Get the current time
      this.currentTime =  moment( ).format('ddd hh:mm A');

      // Get the current date
      this.currentDate = moment( this.currentWeather.dt * 1000 ).format('MMM DD YYYY');

      // Remove the current item from the list
      data.list.shift();

      // Format the time
      this.weathers = data.list.map( day => {
        day.dt = moment( day.dt * 1000 ).format('ddd');
        return day;
      });

      // Get the city info
      this.city = data.city;
    });
  }

  getWeatherData() {

    const params = {
      q: 'New York',
      mode: 'json',
      units: 'imperial',
      cnt: 6,
      appid: 'beb6e661223ea96faa49a33e12dca413'
    };

    // Build the url
    const url = 'http://api.openweathermap.org/data/2.5/forecast/daily?' + this.serialize(params);

    return this.http.get( url ).map( res => {
      const x = res.json();
      return x;
    });

  }

  serialize(obj: any) {
    // Remove all null values in an object
    Object.keys(obj).forEach((key) => (obj[key] === undefined || obj[key] === null) && delete obj[key]);

    // Minified solution from http://stackoverflow.com/questions/1714786/querystring-encoding-of-a-javascript-object
    return Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
  }

}
